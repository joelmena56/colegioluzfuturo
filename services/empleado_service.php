<?php
require_once 'models/empleado_model.php';
require_once 'lib/data_manager.php';
require_once 'lib/util.php';

class EmpleadoService extends DataManager
{
    public function registrar($_item)
    {
        $r = false;
        $fechaInicio = Util::standardizeDate($_item->FechaInicio);
        $sql = "call sp_registrarEmpleado(";
        $sql .= "'$_item->Nombre',";
        $sql .= "'$_item->Apellidos',";
        $sql .= "'$_item->Telefono ',";
        $sql .= "'$_item->Cedula',";
        $sql .= "'$_item->Sexo',";
        $sql .= "'$_item->Direccion',";
        $sql .= "'$_item->Correo',";
        $sql .= "$_item->IdPuesto,";
        $sql .= "'$fechaInicio',";
        $sql .= "$_item->IdAreaDocente,";
        $sql .= "$_item->IdNivelEscolar";
        $sql .= ");";

        $r = $this->executeNonQuery($sql);
        return $r;
    }

    public function buscar($_busqueda)
    {
        $empleados = array();
        $sql = "call sp_buscarEmpleados('$_busqueda');";

        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($empleados, $row);
            }
        }
        return json_encode($empleados);
    }
    //
    public function detalle($_id)
    {
        $sql = "call sp_detalleEmpleado($_id);";

        $row = array();
        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            if ($row = $result->fetch_assoc()) {
                $fecha = new DateTIme($row['FechaInicio']);
                $row['FechaInicio'] = $fecha->format("d/m/Y");
                return json_encode($row);
            } else {
                return 0;
            }

        } else {
            return 0;
        }

    }
    //
    public function actualizar($_item)
    {
        $r = false;
        $fechaInicio = Util::standardizeDate($_item->FechaInicio);
        $sql = "call sp_actualizarDatosEmpleado(";
        $sql .= "$_item->Id,";
        $sql .= "'$_item->Nombre',";
        $sql .= "'$_item->Apellidos',";
        $sql .= "'$_item->Telefono',";
        $sql .= "'$_item->Cedula',";
        $sql .= "'$_item->Sexo',";
        $sql .= "'$_item->Direccion',";
        $sql .= "'$_item->Correo',";
        $sql .= "$_item->IdPuesto,";
        $sql .= "'$fechaInicio',";
        $sql .= "$_item->IdAreaDocente,";
        $sql .= "$_item->IdNivelEscolar";
        $sql .= ");";

        $r = $this->executeNonQuery($sql);
        return $r;
    }

    public function buscarParaAsignar($_datos)
    {
        $items = array();
        $horaFin = date('H:i:s', strtotime($_datos->Hora . ' +45 minutes'));
        $sql = "call sp_listaProfDisponibles(";
        $sql .= "'$_datos->Hora',";
        $sql .= "'$horaFin',";
        $sql .= "$_datos->Dia,";
        $sql .= "'$_datos->Busqueda'";
        $sql .= ");";

        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $t = new Tutor();

                $t->Id = $row['Id'];
                $t->Nombre = $row['Nombre'];
                $t->Apellidos = $row['Apellidos'];

                array_push($items, $row);
            }
        }
        return json_encode($items);
    }
}
