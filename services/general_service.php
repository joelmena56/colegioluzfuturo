<?php
require_once 'lib/data_manager.php';

class GeneralService extends DataManager
{
    public function listaPuestos(){
        $sql = "call sp_listaPuestos();";
        $result = $this->executeQuery($sql);
        $puestos = Array();

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($puestos,$row);
            }
        }
        return $puestos;
    }

    public function agregarAula($_nombreAula, $_ubicacionAula)
    {
        $sql = "call sp_agregarAula('$_nombreAula','$_ubicacionAula')";
        if($this->executeNonQuery($sql) > 0){
            echo "Aula registrada satisfactoriamente";
        }
    }

    public function listaAulas()
    {
        $aulas = array();
        $sql = "call sp_listaAulas();";

        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($aulas, $row);
            }
        }
        return $aulas;
    }

    public function listaAreasDocentes()
    {
        $areas = array();
        $sql = "call sp_listaAreasDocentes();";

        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($areas, $row);
            }
        }
        return $areas;
    }
}
