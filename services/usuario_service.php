<?php
require_once 'models/persona_model.php';
require_once 'lib/data_manager.php';
require_once 'models/usuario_model.php';

class UsuarioService extends DataManager
{
    public function registrarNuevo($_usuario)
    {

    }
    //
    public function buscar($_busqueda)
    {
        $elements = array();
        $sql = "call sp_buscarUsuarios('$_busqueda');";

        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($elements, $row);
            }
        }
        return json_encode($elements);
    }
    //
    public function validar($_nombreUsuario, $_password)
    {
        $sql = "call sp_login('$_nombreUsuario','$_password');";
        $row = null;
        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            if ($row = $result->fetch_assoc()) {
                return $row;
            }
        }
    }

    //
    public function buscarContactos($_busqueda)
    {
        $elements = array();
        $sql = "call sp_buscarContactosParaAsignar('$_busqueda');";

        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($elements, $row);
            }
        }
        return json_encode($elements);
    }
    //
    public function registrar($_item)
    {
        $r = false;
        $sql = "call sp_registrarUsuario(";
        $sql .= "$_item->IdPersona,";
        $sql .= "'$_item->NombreUsuario',";
        $sql .= "'$_item->Password ',";
        $sql .= "$_item->IdTipoUsuario";
        $sql .= ");";

        $r = $this->executeNonQuery($sql);
        return $r;
    }

}
