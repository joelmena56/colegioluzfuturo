<?php
require_once 'models/estudiante_model.php';
require_once 'lib/data_manager.php';
require_once 'lib/util.php';

class EstudianteService extends DataManager
{
    public function registrar($_estudiante)
    {
        $r = false;
        $fechaNacimiento = Util::standardizeDate($_estudiante->FechaNacimiento);
        $sql = "call sp_registrarEstudiante(";
        $sql .= "'$_estudiante->Nombre',";
        $sql .= "'$_estudiante->Apellidos',";
        $sql .= "'$_estudiante->Telefono ',";
        $sql .= "'$_estudiante->Sexo',";
        $sql .= "'$fechaNacimiento',";
        $sql .= "'$_estudiante->Direccion ',";
        $sql .= "'$_estudiante->Email',";
        $sql .= "$_estudiante->IdPadre,";
        $sql .= "$_estudiante->IdMadre,";
        $sql .= "$_estudiante->IdCurso";
        $sql .= ");";

        $r = $this->executeNonQuery($sql);
        return $r;
    }
    //
    public function actualizar($_estudiante)
    {
        $r = false;
        $fechaNacimiento = Util::standardizeDate($_estudiante->FechaNacimiento);
        $sql = "call sp_actualizarDatosEstudiante(";
        $sql .= "$_estudiante->Id,";
        $sql .= "'$_estudiante->Nombre',";
        $sql .= "'$_estudiante->Apellidos',";
        $sql .= "'$_estudiante->Telefono ',";
        $sql .= "'$_estudiante->Sexo',";
        $sql .= "'$fechaNacimiento',";
        $sql .= "'$_estudiante->Direccion ',";
        $sql .= "'$_estudiante->Email',";
        $sql .= "$_estudiante->IdPadre,";
        $sql .= "$_estudiante->IdMadre,";
        $sql .= "$_estudiante->IdCurso";
        $sql .= ");";

        $r = $this->executeNonQuery($sql);
        return $r;
    }

    public function buscar($_busqueda)
    {
        $estudiantes = array();
        $sql = "call sp_buscarEstudiantes('$_busqueda');";

        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($estudiantes, $row);
            }
        }
        return json_encode($estudiantes);
    }
    //
    public function detalle($_id)
    {
        $sql = "call sp_detalleEstudiante($_id);";

        $row = array();
        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            if ($row = $result->fetch_assoc()) {
                $fecha = new DateTime($row['FechaNacimiento']);
                $row['FechaNacimiento'] = $fecha->format("d/m/Y");
                return json_encode($row);
            }
        }
    }
    //
}
