<?php
require_once 'lib/data_manager.php';

class CursoService extends DataManager
{
    public function registrar($_curso)
    {
        $r = false;
        $sql = "call sp_registrarCurso(";
        $sql .= "'$_curso->Nombre',";
        $sql .= "$_curso->IdAula,";
        $sql .= "$_curso->IdTanda,";
        $sql .= "$_curso->IdCiclo";
        $sql .= ");";

        $r = $this->executeNonQuery($sql);
        return $r;
    }
    //
    public function buscar($_busqueda)
    {
        $sql = "call sp_buscarCursos('$_busqueda');";
        $result = $this->executeQuery($sql);
        $cursos = array();

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($cursos, $row);
            }
        }
        return $cursos;
    }
    //
    public function listado()
    {
        $sql = "call sp_listaCursos();";
        $result = $this->executeQuery($sql);
        $cursos = array();

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                array_push($cursos, $row);
            }
        }
        return $cursos;
    }

    //
    public function detalle($_id)
    {
        $sql = "call sp_detalleCurso($_id);";

        $row = array();
        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            if ($row = $result->fetch_assoc()) {
                return json_encode($row);
            } else {
                return 0;
            }
        } else {
            return 0;
        }

    }
    //
    //
    public function comprobarConflictosHorariosProfesor($_item)
    {
        $conflictos = array();
        $horaFin = date('H:i:s', strtotime($_item->Hora . ' +45 minutes'));
        $sql = "call sp_conflictosHorariosProfesor($_item->IdProfesor,'$_item->Hora','$horaFin',$_item->Dia);";
        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            if ($row = $result->fetch_assoc()) {
                $row['Hora'] = Util::get12HTime($row['Hora']);
                $day = Util::getDayName($row['Dia']);
                $row['Dia'] = $day;
                array_push($conflictos, $row);
            }
        } else { $conflictos = 0;}
        return $conflictos;
    }
    //
    //
    public function comprobarConflictosHorarios($_item)
    {
        $conflictos = 0;
        $horaFin = date('H:i:s', strtotime($_item->Hora . ' +45 minutes'));
        $sql = "call ingenieria2.sp_conflictosHorarioCuros($_item->IdCurso,$_item->Dia,'$_item->Hora','$horaFin');";

        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            if ($row = $result->fetch_assoc()) {
                $conflictos = $row['CantHorarios'];
            }
        }
        return $conflictos;
    }
    //
    public function listaHorarios($_id)
    {
        $sql = "call sp_listaHorariosCurso($_id);";
        $result = $this->executeQuery($sql);
        $elements = array();

        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $row['Hora'] = Util::get12HTime($row['Hora']);
                $day = Util::getDayName($row['Dia']);
                $row['Dia'] = $day;
                array_push($elements, $row);
            }
        }
        return json_encode($elements);
    }
    //

    public function registrarHorario($_item)
    {
        $r = false;
        $sql = "call sp_registrarHorario(";
        $sql .= "$_item->IdCurso,";
        $sql .= "'$_item->Hora',";
        $sql .= "$_item->IdProfesor,";
        $sql .= "$_item->Dia,";
        $sql .= "$_item->IdAsignatura";
        $sql .= ");";
        $r = $this->executeNonQuery($sql);
        return $r;
    }
    //

    public function eliminarHorario($_id)
    {
        $r = false;
        $sql = "call sp_eliminarHorario($_id)";
        $r = $this->executeNonQuery($sql);
        return $r;
    }
    //
    public function listadoAsistencia($_idCurso, $_fecha)
    {
        $fecha = Util::standardizeDate($_fecha);
        $estado['A'] = "Ausente";
        $estado['E'] = "Excusa";
        $estado['T'] = "Tarde";
        $estado['P'] = "Presente";
        $estado['PEND'] = "Pendiente";

        $sql = "call sp_listaAsistencia($_idCurso,'$fecha')";
        $elements = array();
        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $row['Estado'] = $estado[$row['Estado']];
                array_push($elements, $row);
            }
        }
        return json_encode($elements);
    }
    //
    //
    public function actualizarEstadoAsistencia($_idCurso, $_estado)
    {
        $sql = "call sp_actualizarListaAsistencia($_idCurso,'$_estado')";
        $r = false;
        $r = $this->executeNonQuery($sql);
        return $r;
    }
    //
}
