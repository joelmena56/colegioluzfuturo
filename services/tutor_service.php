<?php
require_once 'models/tutor_model.php';
require_once 'lib/data_manager.php';

class TutorService extends DataManager
{
    public function registrarNuevo($_tutor)
    {
        $r = false;
        $sql = "call sp_registrarTutor(";
        $sql .= "'$_tutor->Nombre',";
        $sql .= "'$_tutor->Apellidos',";
        $sql .= "'$_tutor->Telefono ',";
        $sql .= "'$_tutor->Cedula',";
        $sql .= "'$_tutor->Sexo',";
        $sql .= "'$_tutor->Direccion ',";
        $sql .= "'$_tutor->Correo'";
        $sql .= ");";

        $r = $this->executeNonQuery($sql);
        return $r;
    }
    //
    public function buscar($_busqueda)
    {
        $tutores = array();
        $sql = "call sp_buscarTutores('$_busqueda');";

        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $t = new Tutor();

                $t->Id = $row['Id'];
                $t->Nombre = $row['Nombre'];
                $t->Apellidos = $row['Apellidos'];
                $t->Cedula = $row['Cedula'];
                $t->Telefono = $row['Telefono'];

                array_push($tutores, $t);
            }
        }
        return $tutores;
    }
    //
    public function buscarParaAsignar($_busqueda,$_sexo)
    {
        $tutores = array();
        $sql = "call sp_buscarTutoresParaAsignar('$_busqueda','$_sexo');";

        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            while ($row = $result->fetch_assoc()) {
                $t = new Tutor();

                $t->Id = $row['Id'];
                $t->Nombre = $row['Nombre'];
                $t->Apellidos = $row['Apellidos'];

                array_push($tutores, $t);
            }
        }
        return $tutores;
    }
    //
    public function detalle($_id)
    {
        $sql = "call sp_detalleTutor($_id);";

        $t = new Tutor();
        $result = $this->executeQuery($sql);
        if ($result->num_rows > 0) {
            if ($row = $result->fetch_assoc()) {
                $t->Id = $row['Id'];
                $t->Nombre = $row['Nombre'];
                $t->Apellidos = $row['Apellidos'];
                $t->Cedula = $row['Cedula'];
                $t->Telefono = $row['Telefono'];
                $t->Cedula = $row['Cedula'];
                $t->Sexo = $row['Sexo'];
                $t->Direccion = $row['Direccion'];
                $t->Correo = $row['Correo'];
            }
        }
        return $t;
    }
    //
    public function actualizar($_tutor)
    {
        $sql = "call sp_actualizarDatosTutor(";
        $sql .= "$_tutor->Id,";
        $sql .= "'$_tutor->Nombre',";
        $sql .= "'$_tutor->Apellidos',";
        $sql .= "'$_tutor->Telefono',";
        $sql .= "'$_tutor->Cedula',";
        $sql .= "'$_tutor->Sexo',";
        $sql .= "'$_tutor->Direccion',";
        $sql .= "'$_tutor->Correo'";
        $sql .= ");";

        $this->executeNonQuery($sql);
    }
}
