<?php
class Util
{
    public static function getJsonFileData($_fileName)
    {
        $jsonStr = file_get_contents($_fileName);

        $decodedjsonStr = html_entity_decode($jsonStr);
        $r = json_decode($decodedjsonStr);
        return $r;
    }
    //
    public static function getObjectFromJsonStr($_jsonStr)
    {
        $decodedjsonStr = html_entity_decode($_jsonStr);
        $r = json_decode($decodedjsonStr);
        return $r;
    }
    //
    public static function standardizeDate($_strDate)
    {
        $formatter = new IntlDateFormatter("es", IntlDateFormatter::SHORT, IntlDateFormatter::NONE);
        $unixtime = $formatter->parse($_strDate);
        $datetime = new DateTime();
        $datetime->setTimestamp($unixtime);
        return $datetime->format('Y-m-d');
    }
    //
    public static function getDayName($_dayNumber){
        $days = ['Lunes','Martes','Miercoles','Jueves','Viernes','Sabado','Domingo'];
        return $days[$_dayNumber - 1];
    }
    //
    public static function get12HTime($_hora){
        return date('h:i:s A', strtotime($_hora));
    }

}
