<?php
class Security
{
    public static function logIn()
    {
        session_start();
        $r = false;
        if (isset($_SESSION['idUsuario'])) {
            $r = true;
        }
        return $r;
    }
    //
    public static function init()
    {
        session_start();
    }
    //
    public static function logOut()
    {
        session_start();
        session_destroy();
    }
}
