<?php
class ObjectMaker
{
    /*Definition of all  controllers to be used*/
    public static function getController($_controllerName)
    {
        require_once "controllers/home_controller.php";
        require_once "controllers/estudiante_controller.php";
        require_once "controllers/empleado_controller.php";
        require_once "controllers/padres_controller.php";
        require_once "controllers/curso_controller.php";
        require_once "controllers/mantenimiento_controller.php";

        //
        $object = null;
        switch ($_controllerName) {
            case 'home':
                $object = new HomeController();
                break;
            case 'estudiante':
                $object = new EstudianteController();
                break;
            case 'empleado':
                $object = new EmpleadoController();
                break;
            case 'padres':
                $object = new PadresController();
                break;
            case 'curso':
                $object = new CursoController();
                break;
            case 'mantenimiento':
                $object = new MantenimientoController();
                break;
        }
        return $object;
    }
    /*Definition of all  views to be used*/
    public static function getView($_viewName, $_data)
    {
        $object = null;
        switch ($_viewName) {
            case 'index':
                $object = new IndexView($_data);
                break;
            case 'login':
                $object = new LoginView($_data);
                break;
            case 'estudianteIndex':
                $object = new EstudianteIndexView($_data);
                break;
            case 'empleadoIndex':
                $object = new EmpleadoIndexView($_data);
                break;
            case 'padresIndex':
                $object = new PadresIndexView($_data);
                break;
            case 'cursoIndex':
                $object = new CursoIndexView($_data);
                break;
            case 'cursoAsistencia':
                $object = new CursoAsistenciaView($_data);
                break;
            case 'mantenimientoIndex':
                $object = new MantenimientoIndexView($_data);
                break;
        }
        return $object;
    }
}
