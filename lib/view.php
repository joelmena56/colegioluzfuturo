<?php
abstract class View
{
    //============MEMBER PROPERIES=====================
    protected $data;
    protected $html;
    protected $dictionary;

    //
    protected function prepareViewsData()
    {
        $this->html = "Empty view";
    }
    public function displayHtml()
    {
        $this->prepareViewsData();
        print($this->html);
    }
}
