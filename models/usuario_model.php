<?php
require_once 'models/persona_model.php';
class Usuario extends Persona
{
    public $Id;
    public $IdPersona;
    public $NombreUsuario;
    public $Password;
    public $Administrador;
    //
    public function toJSON()
    {
        return json_encode([
            'Id' => $this->Id,
            'Nombre' => $this->Nombre,
            'Apellidos' => $this->Apellidos,
            'NombreUsuario' => $this->NombreUsuario,
            'IdPersona' => $this->IdPersona,
            'Administrador' => $this->Administrador
        ]);
    }
}
