<?php
require_once 'models/persona_model.php';
class Empleado extends Persona
{
    public $IdPuesto;
    public $FehcaInicio;
    public $IdAreaDocente;
    public $IdNivelEscolar;

    public function toJson()
    {
        return json_encode([
            'Id' => $this->Id,
            'Nombre' => $this->Nombre,
            'Apellidos' => $this->Apellidos,
            'Cedula' => $this->Cedula,
            'FechaInicio' => $this->FehcaInicio,
            'Telefono' => $this->Telefono,
            'Sexo' => $this->Sexo,
            'Correo' => $this->Correo,
            'Direccion' => $this->Direccion,
            'IdPuesto' => $this->IdPuesto,
            'IdAreaDocente' => $this->IdAreaDocente,
            'IdNivelEscolar' => $this->IdNivelEscolar
        ]);
    }
}
