<?php
abstract class Persona
{
    public $Id;
    public $Nombre;
    public $Apellidos;
    public $FechaNacimiento;
    public $Telefono;
    public $Cedula;
    public $Sexo;
    public $Correo;
    public $Direccion;
    //
    public abstract function toJSON();
}
