<?php
require_once 'models/persona_model.php';

class Tutor extends Persona
{
    public function toJSON()
    {
        return json_encode([
            'Id' => $this->Id,
            'Nombre' => $this->Nombre,
            'Apellidos' => $this->Apellidos,
            'Cedula' => $this->Cedula,
            'Telefono' => $this->Telefono,
            'Sexo' => $this->Sexo,
            'Correo' => $this->Correo,
            'Direccion' => $this->Direccion,
        ]);

    }
}
