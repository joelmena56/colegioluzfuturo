<?php
require_once 'models/persona_model.php';
class Estudiante extends Persona
{
    public $Nombre;
    public $Apellidos;
    public $Telefono;
    public $FechaNacimiento;
    public $Sexo;
    public $Correo;
    public $Direccion;
    public $IdPadre;
    public $IdMadre;
    public $NombreMadre;
    public $NombrePadre;

    public function toJson()
    {
        return json_encode([
            'Id' => $this->Id,
            'Nombre' => $this->Nombre,
            'Apellidos' => $this->Apellidos,
            'Cedula' => $this->Cedula,
            'FechaNacimiento' => $this->FechaNacimiento->format('d/m/Y'),
            'Telefono' => $this->Telefono,
            'Sexo' => $this->Sexo,
            'Correo' => $this->Correo,
            'Direccion' => $this->Direccion,
            'IdPadre' => $this->IdPadre,
            'IdMadre' => $this->IdMadre,
            'NombrePadre' => $this->NombrePadre,
            'NombreMadre' => $this->NombreMadre
        ]);
    }
}
