<?php

require_once 'interfaces/controller_interface.php';
require_once 'lib/object_maker.php';
require_once 'views/mantenimiento/index_view.php';
require_once 'services/general_service.php';
require_once 'services/usuario_service.php';

class MantenimientoController implements IController
{
    //============PUBLIC METHODS=======================
    public function executeAction($_actionName)
    {
        switch ($_actionName) {
            case "index":
                $this->index();
                break;
            case "agregarAula":
                $this->agregarAula();
                break;
            case "listaAulas":
                $this->listaAulas();
                break;
            case "buscarUsuarios":
                $this->buscarUsuarios();
                break;
            case "buscarContactos":
                $this->buscarContactos();
                break;
            case "registrarUsuario":
                $this->registrarUsuario();
                break;
        }
    }

    //============ACTIONS==============================
    private function index()
    {
        if (Security::logIn()) {
            $data = null;
            $view = ObjectMaker::getView('mantenimientoIndex', $data);
            $view->displayHtml();
        } else {
            header("location: ?ctrl=home&action=login");
        }

    }
    //
    private function agregarAula()
    {
        if (isset($_POST['nombreAula']) && isset($_POST['ubicacionAula'])) {
            $nombreAula = $_POST['nombreAula'];
            $ubicacionAula = $_POST['ubicacionAula'];

            $gs = new GeneralService();
            $gs->agregarAula($nombreAula, $ubicacionAula);
        }
    }

    private function listaAulas()
    {
        $gs = new GeneralService();
        $aulas = $gs->listaAulas();

        echo json_encode($aulas);
    }

    private function buscarUsuarios()
    {
        if (isset($_POST['busqueda'])) {
            $busqueda = $_POST['busqueda'];
            $us = new UsuarioService();
            echo $us->buscar($busqueda);
        }
    }

    private function buscarContactos()
    {
        if (isset($_POST['busqueda'])) {
            $busqueda = $_POST['busqueda'];
            $us = new UsuarioService();
            echo $us->buscarContactos($busqueda);
        }
    }

        //
        private function registrarUsuario()
        {
            if (isset($_POST['datos'])) {
                $datos = $_POST['datos'];
                $element = Util::getObjectFromJsonStr($datos);
                $us = new UsuarioService();
                
                if ($us->registrar($element)) {
                    echo 'Usuario registrado satisfactoriamente';
                } else {
                    echo 'Hubo un error al intentar registrar Usuario';
                }
            }
        }
        //
}
