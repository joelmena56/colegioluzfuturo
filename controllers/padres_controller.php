<?php

require_once 'interfaces/controller_interface.php';
require_once 'lib/object_maker.php';
require_once 'lib/util.php';
require_once 'services/tutor_service.php';
require_once 'views/padres/index_view.php';

class PadresController implements IController
{
    //============PUBLIC METHODS=======================
    public function executeAction($_actionName)
    {
        switch ($_actionName) {
            case "index":
                $this->index();
                break;
            case "registrar":
                $this->guardarPersona();
                break;
            case "buscar":
                $this->buscar();
                break;
            case "detalle":
                $this->detalle();
                break;
            case "actualizar":
                $this->actualizar();
                break;
            case "buscarParaAsignar":
                $this->buscarParaAsignar();
                break;
        }
    }

    //============ACTIONS==============================
    private function index()
    {
        if (Security::logIn()) {
            $data = null;
            $view = ObjectMaker::getView('padresIndex', $data);
            $view->displayHtml();

        } else {
            header("location: ?ctrl=home&action=login");
        }
    }
    //
    private function guardarPersona()
    {
        if (isset($_POST['datos'])) {
            $datos = $_POST['datos'];
            $p = Util::getObjectFromJsonStr($datos);
            $ts = new TutorService();
            if ($ts->registrarNuevo($p)) {
                echo '1';
            } else {
                echo '0';
            }
        }

    }
    //
    private function buscar()
    {
        if (isset($_POST['busqueda'])) {
            $busqueda = $_POST['busqueda'];
            $ts = new TutorService();
            $tutores = $ts->buscar($busqueda);
            // var_dump($tutores);
            $jsonArray = array();
            foreach ($tutores as $t) {
                array_push($jsonArray, $t->toJson());
            }
            echo json_encode($jsonArray);
        }
    }
    //
    private function buscarParaAsignar()
    {
        if (isset($_POST['busqueda'])&& isset($_POST['sexo'])) {
            $busqueda = $_POST['busqueda'];
            $sexo = $_POST['sexo'];

            $ts = new TutorService();
            $tutores = $ts->buscarParaAsignar($busqueda,$sexo);
            $jsonArray = array();
            foreach ($tutores as $t) {
                array_push($jsonArray, $t->toJson());
            }
            echo json_encode($jsonArray);
        }
    }
    //
    private function detalle()
    {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $ts = new TutorService();
            $t = new Tutor();
            $t = $ts->detalle($id);
            echo $t->toJSON();

        }
    }
    //
    private function actualizar()
    {
        if (Security::logIn()) {
            if (isset($_POST['datos'])) {
                $datos = $_POST['datos'];
                $ts = new TutorService();
                $datos = Util::getObjectFromJsonStr($datos);
                $ts->actualizar($datos);
            }
        } else {
            header("location: ?ctrl=home&action=login");
        }
    }
}
