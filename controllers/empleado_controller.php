<?php

require_once 'interfaces/controller_interface.php';
require_once 'lib/object_maker.php';
require_once 'views/empleados/index_view.php';
require_once 'services/general_service.php';
require_once 'services/empleado_service.php';

class EmpleadoController implements IController
{
    //============PUBLIC METHODS=======================
    public function executeAction($_actionName)
    {
        switch ($_actionName) {
            case "index":
                $this->index();
                break;
            case "registrar":
                $this->registrar();
                break;
            case "buscar":
                $this->buscar();
                break;
            case "detalle":
                $this->detalle();
                break;
            case "actualizar":
                $this->actualizar();
                break;
            case "buscarParaAsignar":
                $this->buscarParaAsignar();
                break;
        }

    }

    //============ACTIONS==============================
    private function index()
    {
        if (Security::logIn()) {
            $data = null;
            $gs = new GeneralService();
            $data['puestos'] = $gs->listaPuestos();
            $data['areas'] = $gs->listaAreasDocentes();
            $view = ObjectMaker::getView('empleadoIndex', $data);
            $view->displayHtml();
        } else {
            header("location: ?ctrl=home&action=login");
        }

    }
    //
    private function registrar()
    {
        if (isset($_POST['datos'])) {
            $datos = $_POST['datos'];
            $e = Util::getObjectFromJsonStr($datos);
            $es = new EmpleadoService();

            if ($es->registrar($e)) {
                echo 'Empleado registrado satisfactoriamente';
            } else {
                echo 'Hubo un error al intentar registrar Empleado';
            }
        }
    }
    //
    private function buscar()
    {
        if (isset($_POST['busqueda'])) {
            $busqueda = $_POST['busqueda'];
            $es = new EmpleadoService();
            $jsItem = $es->buscar($busqueda);
            echo $jsItem;
        }
    }

    private function detalle()
    {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $es = new EmpleadoService();
            echo $es->detalle($id);
        }
    }
    //
    private function actualizar()
    {
        if (isset($_POST['datos'])) {
            $datos = $_POST['datos'];
            $e = Util::getObjectFromJsonStr($datos);
            $es = new EmpleadoService();

            if ($es->actualizar($e)) {
                echo 'Datos de empleado actualizados satisfactoriamente';
            } else {
                echo 'Hubo un error al intentar actualizar datos del empleado';
            }
        }
    }
    //
    private function buscarParaAsignar()
    {
        if (isset($_POST['datos'])) {
            $datos = $_POST['datos'];
            $es = new EmpleadoService();
            echo $es->buscarParaAsignar(Util::getObjectFromJsonStr($datos));
        }
    }
    //
}
