<?php

require_once 'interfaces/controller_interface.php';
require_once 'lib/object_maker.php';
require_once 'views/cursos/index_view.php';
require_once 'views/cursos/asistencia_view.php';
require_once 'services/general_service.php';
require_once 'services/curso_service.php';

class CursoController implements IController
{
    //============PUBLIC METHODS=======================
    public function executeAction($_actionName)
    {
        switch ($_actionName) {
            case "index":
                $this->index();
                break;
            case "asistencia":
                $this->asistencia();
                break;
            case "registrar":
                $this->registrar();
                break;
            case "buscar":
                $this->buscar();
                break;
            case "detalle":
                $this->detalle();
                break;
            case "registrarHorario":
                $this->registrarHorario();
                break;
            case "listaHorarios":
                $this->listaHorarios();
                break;
            case "eliminarHorario":
                $this->eliminarHorario();
                break;
            case "listadoAsistencia":
                $this->listadoAsistencia();
                break;
            case "actualizarEstadoAsistencia":
                $this->actualizarEstadoAsistencia();
                break;
        }
    }

    //============ACTIONS==============================
    private function index()
    {
        if (Security::logIn()) {
            $gs = new GeneralService();
            $data['aulas'] = $gs->listaAulas();
            $data['areas'] = $gs->listaAreasDocentes();
            $view = ObjectMaker::getView('cursoIndex', $data);
            $view->displayHtml();
        } else {
            header("location: ?ctrl=home&action=login");
        }
    }
    //
    private function asistencia()
    {
        $data = null;
        $view = ObjectMaker::getView('cursoAsistencia', $data);
        $view->displayHtml();
    }
    //
    //
    private function registrar()
    {
        if (isset($_POST['datos'])) {
            $datos = $_POST['datos'];
            $c = Util::getObjectFromJsonStr($datos);
            $cs = new CursoService();

            if ($cs->registrar($c)) {
                echo 'Curso registrado satisfactoriamente';
            } else {
                echo 'Hubo un error al intentar registrar curso';
            }
        }
    }
    //
    private function buscar()
    {
        if (isset($_POST['busqueda'])) {
            $busqueda = $_POST['busqueda'];
            $cs = new CursoService();
            $cursos = $cs->buscar($busqueda);
            echo json_encode($cursos);
        }
    }

    private function detalle()
    {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $cs = new CursoService();
            echo $cs->detalle($id);
        }
    }

    private function registrarHorario()
    {
        if (isset($_POST['datos'])) {
            $datos = $_POST['datos'];
            $datos = Util::getObjectFromJsonStr($datos);
            $cs = new CursoService();
            $conflictosProf = $cs->comprobarConflictosHorariosProfesor($datos);
            if ($conflictosProf == '0') {
                if ($cs->comprobarConflictosHorarios($datos) > 0) {
                    echo "El horario que intenta agregar tiene conflicto con uno mas horarios de la lista";
                } else {
                    if ($cs->registrarHorario($datos)) {
                        echo ("Horario agregado satisfactoriamente");
                    } else {
                        echo "Error al agregar horario";
                    }
                }
            } else {
                $mensaje = "El profesor seleccionado tiene los siguientes conflictos de horario: \n";
                foreach ($conflictosProf as $item) {
                    $mensaje .= "$item[NombreCurso], $item[Dia], $item[Hora]\n";
                }
                echo $mensaje;
            }
        }
    }

    private function listaHorarios()
    {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $cs = new CursoService();
            echo $cs->listaHorarios($id);
        }
    }

    private function eliminarHorario()
    {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $id = Util::getObjectFromJsonStr($id);
            $cs = new CursoService();
            if ($cs->eliminarHorario($id)) {
                echo "Se ha eliminado el Horario correctamente";
            } else {
                echo "Hubo un error al intentar eliminar Horario";
            }
        }
    }

    private function listadoAsistencia()
    {
        if (isset($_POST['idCurso']) && isset($_POST['fecha'])) {
            $idCurso = $_POST['idCurso'];
            $fecha = $_POST['fecha'];

            $cs = new CursoService();
            echo $cs->listadoAsistencia($idCurso, $fecha);
        }
    }
    //
    private function actualizarEstadoAsistencia()
    {
        if (isset($_POST['id']) && isset($_POST['estado'])) {
            $id = $_POST['id'];
            $estado = $_POST['estado'];

            $cs = new CursoService();
            $cs->actualizarEstadoAsistencia($id,$estado);
        }
    }
    //
}
