<?php

require_once 'interfaces/controller_interface.php';
require_once 'lib/object_maker.php';
require_once 'views/estudiantes/index_view.php';
require_once 'services/estudiante_service.php';
require_once 'services/curso_service.php';

class EstudianteController implements IController
{
    //============PUBLIC METHODS=======================
    public function executeAction($_actionName)
    {
        switch ($_actionName) {
            case "index":
                $this->index();
                break;
            case "registrar":
                $this->registrar();
                break;
            case "buscar":
                $this->buscar();
            case "detalle":
                $this->detalle();
                break;
            case "actualizar":
                $this->actualizar();
                break;
        }
    }

    //============ACTIONS==============================
    private function index()
    {
        if (Security::logIn()) {

            $cs = new CursoService();
            $data['cursos'] = $cs->listado();
            $view = ObjectMaker::getView('estudianteIndex', $data);
            $view->displayHtml();
        } else {
            header("location: ?ctrl=home&action=login");
        }
    }
    //
    private function buscar()
    {
        if (isset($_POST['busqueda'])) {
            $busqueda = $_POST['busqueda'];
            $es = new EstudianteService();
            $jsEstudiantes= $es->buscar($busqueda);
            echo $jsEstudiantes;
        }
    }
    //
    private function registrar()
    {
        if (isset($_POST['datos'])) {
            $datos = $_POST['datos'];
            $e = Util::getObjectFromJsonStr($datos);
            $es = new EstudianteService();

            if ($es->registrar($e)) {
                echo 'Estudiante registrado satisfactoriamente';
            } else {
                echo 'Hubo un error al intentar registrar Estudiante';
            }
        }
    }
    //
    private function detalle()
    {
        if (isset($_POST['id'])) {
            $id = $_POST['id'];
            $es = new EstudianteService();
            echo $es->detalle($id);
        }
    }
    //
    private function actualizar()
    {
        if (isset($_POST['datos'])) {
            $datos = $_POST['datos'];
            $e = Util::getObjectFromJsonStr($datos);
            $es = new EstudianteService();
            if ($es->actualizar($e)) {
                echo 'Estudiante actualizado satisfactoriamente';
            } else {
                echo 'Hubo un error al intentar actualizar Estudiante';
            }
        }
    }
}
