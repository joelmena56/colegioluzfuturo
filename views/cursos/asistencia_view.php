<?php

require_once 'lib/view_render.php';
require_once 'lib/view.php';

class CursoAsistenciaView extends View
{

    public function __construct($_data)
    {
        $this->data = $_data;
        $this->header = "";
    }
    //============PUBLIC METHODS=======================
    protected function prepareViewsData()
    {
        $this->dictionary = null;
        $sidePanelDictionary = array(
            'CURSO_MENU' => "active"
        );

        $htmlAreas = "";
        foreach ($this->data['areas'] as $value) {
            $htmlAreas .= "<option value=$value[Id]>$value[Nombre]</option>";
        }
        $headContent = ViewRender::getFile("site_media/html/layout/head.html");
        $header = ViewRender::getFile("site_media/html/layout/header.html");
        $sidePanel = ViewRender::renderTemplate("site_media/html/layout/side_panel.html", $sidePanelDictionary);

        $this->dictionary['TITLE'] = "Cursos";
        $this->dictionary['HEAD_CONTENT'] = $headContent;
        $this->dictionary['HEADER'] = $header;
        $this->dictionary['SIDE_PANEL'] = $sidePanel;
        $this->dictionary['AREAS_DOCENTES'] = $htmlAreas;

        $this->html = ViewRender::renderTemplate("site_media/html/cursos/asistencia.html", $this->dictionary);
    }
}
