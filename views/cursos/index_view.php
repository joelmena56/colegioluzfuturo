<?php

require_once 'lib/view_render.php';
require_once 'lib/view.php';

class CursoIndexView extends View
{

    public function __construct($_data)
    {
        $this->data = $_data;
        $this->header = "";
    }
    //============PUBLIC METHODS=======================
    protected function prepareViewsData()
    {
        $this->dictionary = null;

        $sidePanelDictionary = array(
            'CURSO_MENU' => "active"
        );

        $headerDict['USER_NAME'] = $_SESSION["Nombre"];
        $headerDict['INITIALS'] = $_SESSION['Nombre'][0];
       
        $header = ViewRender::renderTemplate("site_media/html/layout/header.html",$headerDict);
        $headContent = ViewRender::getFile("site_media/html/layout/head.html");
        $sidePanel = ViewRender::renderTemplate("site_media/html/layout/side_panel.html", $sidePanelDictionary);
        $htmlAulas = "";
        foreach ($this->data['aulas'] as $item) {
            $htmlAulas .= "<option value=$item[Id]>$item[Nombre]";
        }
        $htmlAreas = "";
        foreach ($this->data['areas'] as $value) {
            $htmlAreas .= "<option value=$value[Id]>$value[Nombre]</option>";
        }

        $htmlDias = "";
        $htmlDias .= "<option value=1>Lunes</option>";
        $htmlDias .= "<option value=2>Martes</option>";
        $htmlDias .= "<option value=3>Miercoles</option>";
        $htmlDias .= "<option value=4>Jueves</option>";
        $htmlDias .= "<option value=5>Viernes</option>";


        $this->dictionary['TITLE'] = "Cursos";
        $this->dictionary['HEAD_CONTENT'] = $headContent;
        $this->dictionary['HEADER'] = $header;
        $this->dictionary['SIDE_PANEL'] = $sidePanel;
        $this->dictionary['OPTION_AULAS'] = $htmlAulas;
        $this->dictionary['ASIGNATURAS'] = $htmlAreas;
        $this->dictionary['DIAS'] = $htmlDias;

        $this->html = ViewRender::renderTemplate("site_media/html/cursos/index.html", $this->dictionary);
    }
}
