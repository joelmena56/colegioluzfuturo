<?php

require_once 'lib/view_render.php';
require_once 'lib/view.php';

class MantenimientoIndexView extends View
{

    public function __construct($_data)
    {
        $this->data = $_data;
        $this->header = "";
    }
    //============PUBLIC METHODS=======================
    protected function prepareViewsData()
    {
        $this->dictionary = null;
        $sidePanelDictionary = array(
            'MANTENIMIENTO_MENU' => "active"
        );

        $headerDict['USER_NAME'] = $_SESSION["Nombre"];
        $headerDict['INITIALS'] = $_SESSION['Nombre'][0];
       
        $header = ViewRender::renderTemplate("site_media/html/layout/header.html",$headerDict);
        $headContent = ViewRender::getFile("site_media/html/layout/head.html");
        $sidePanel = ViewRender::renderTemplate("site_media/html/layout/side_panel.html", $sidePanelDictionary);

        $this->dictionary['TITLE'] = "Mantenimiento";
        $this->dictionary['HEAD_CONTENT'] = $headContent;
        $this->dictionary['HEADER'] = $header;
        $this->dictionary['SIDE_PANEL'] = $sidePanel;

        $this->html = ViewRender::renderTemplate("site_media/html/mantenimiento/index.html", $this->dictionary);
    }
}
