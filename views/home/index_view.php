<?php
require_once 'lib/view_render.php';
require_once 'lib/view.php';

class IndexView extends View{

    public function __construct($_data) {
        $this->data = $_data;
    }
    //============PUBLIC METHODS=======================
    protected function prepareViewsData() {
        $this->dictionary = NULL;
        $sidePanelDictionary = array(
            'DASHBOARD_MENU' => "active",
            'ESTUDIANTE_MENU' => ""
        );

        $headerDict['USER_NAME'] = $_SESSION["Nombre"];
        $headerDict['INITIALS'] = $_SESSION['Nombre'][0];
       

        $headContent = ViewRender::getFile("site_media/html/layout/head.html");
        $header = ViewRender::renderTemplate("site_media/html/layout/header.html",$headerDict);
        $sidePanel = ViewRender::renderTemplate("site_media/html/layout/side_panel.html", $sidePanelDictionary);

        $this->dictionary['TITLE'] = "Tablero";
        $this->dictionary['HEAD_CONTENT'] = $headContent;
        $this->dictionary['HEADER'] = $header;
        $this->dictionary['SIDE_PANEL'] = $sidePanel;

        $this->html = ViewRender::renderTemplate("site_media/html/home/index.html", $this->dictionary);

    }
}
