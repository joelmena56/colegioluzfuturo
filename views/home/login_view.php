<?php
require_once 'lib/view_render.php';
require_once 'lib/view.php';

class LoginView extends View
{

    public function __construct($_data)
    {
        $this->data = $_data;
    }
    //============PUBLIC METHODS=======================
    protected function prepareViewsData()
    {
        $invalidErrorMessage = "";
        if ($this->data["invalid"]) {
            $invalidErrorMessage = "<p style=color:red;>Usuario y Contraseña no coinciden</p>";
        }

        $this->dictionary = null;
        $headContent = ViewRender::getFile("site_media/html/layout/head.html");
        $this->dictionary = array(
            'HEAD_CONTENT' => $headContent,
            'LOGIN_MESSAGE' => $invalidErrorMessage,
        );
        $this->html = ViewRender::renderTemplate("site_media/html/home/login.html", $this->dictionary);
    }
}
