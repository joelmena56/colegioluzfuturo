<?php

require_once 'lib/view_render.php';
require_once 'lib/view.php';

class EstudianteIndexView extends View
{

    public function __construct($_data)
    {
        $this->data = $_data;
        $this->header = "";
    }
    //============PUBLIC METHODS=======================
    protected function prepareViewsData()
    {
        $this->dictionary = null;
        $sidePanelDictionary = array(
            'DASHBOARD' => "",
            'ESTUDIANTE_MENU' => "active",
        );

        $headerDict['USER_NAME'] = $_SESSION["Nombre"];
        $headerDict['INITIALS'] = $_SESSION['Nombre'][0];
       
        $header = ViewRender::renderTemplate("site_media/html/layout/header.html",$headerDict);
        $headContent = ViewRender::getFile("site_media/html/layout/head.html");
        $sidePanel = ViewRender::renderTemplate("site_media/html/layout/side_panel.html", $sidePanelDictionary);

        $htmlCursos = "";
        foreach ($this->data['cursos'] as $value) {
            $htmlCursos .= "<option value=$value[Id]>$value[Nombre]</option>";
        }

        $this->dictionary['TITLE'] = "Estudiantes";
        $this->dictionary['HEAD_CONTENT'] = $headContent;
        $this->dictionary['HEADER'] = $header;
        $this->dictionary['SIDE_PANEL'] = $sidePanel;
        $this->dictionary['OPTION_CURSOS'] = $htmlCursos;

        $this->html = ViewRender::renderTemplate("site_media/html/estudiantes/index.html", $this->dictionary);
    }
}
