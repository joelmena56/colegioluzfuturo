<?php
interface IView {
    public function displayHtml();
}
