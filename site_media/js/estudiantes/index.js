//[ Variables globales ]
var sexoABuscar = "";
var Estudiante = new Object();

// [ Cuando el documento termine de cargar]
$(function () {
    Estudiante.buscar();

    $("#txtBusqueda").on('keyup', function (e) {
        if (e.keyCode == 13) {
            Estudiante.buscar();
        }
    });

    $("#bntNuevoEstudiante").click(function () {
        DialogPanel.show('divEdicionEstudiante');
        $("#sltCursos").prop("disabled", false);
        $("#h3DialogTitle").html("Inscripción de alumno");
    });

    $("#btnBuscar").click(Estudiante.buscar);

    $("#btnCancelar").click(function () {
        DialogPanel.hide('divEdicionEstudiante')
        Estudiante.Id = 0;
    })

    $("#aExaminarPadres").click(function () {
        $("#divQuickSearch").show();
        $("#txtBusquedaPadres").focus();
        event.stopPropagation();
        sexoABuscar = 'M';
    });

    $("#aExaminarMadres").click(function () {
        $("#divQuickSearch").show();
        $("#txtBusquedaPadres").focus();
        event.stopPropagation();
        sexoABuscar = 'F';
    });

    var timeout;
    $("#txtBusquedaPadres").on("input propertychange", function () {
        clearTimeout(timeout);
        timeout = setTimeout(buscarPadres, 150);
    })

    $("#btnGuardar").click(function () {
        if (Util.requiredValidation("divEdicionEstudiante")) {
            if (Estudiante.Id) {
                Estudiante.actualizarDatos();
            } else {
                Estudiante.registrar();
            }
        }
    });

    $('#txtFechaNacimiento').datepicker({
        format: "dd/mm/yyyy",
        changeMonth: true,
        changeYear: true,
        todayBtn: "linked",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });

});

// [ Funciones principales ] 
function buscarPadres() {
    let busqueda = $("#txtBusquedaPadres").val();
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=padres&action=buscarParaAsignar',
        data: 'busqueda=' + busqueda + '&sexo=' + sexoABuscar + '',
        success: function (result) {
            let html = "";
            result = JSON.parse(result);
            for (a in result) {
                let b = JSON.parse(result[a]);
                let nombre = b.Nombre + " " + b.Apellidos;
                html += '<li> <a onclick=seleccionarTutor(' + b.Id + ',this);>' + nombre + '</a>';
            }
            $("#ulListaPadres").html(html);
        }
    });
}

function seleccionarTutor(_id, anchorItem) {
    if (anchorItem) {
        if (sexoABuscar === 'F') {
            $("#txtNombreMadre").val(anchorItem.innerHTML);
            Estudiante.IdMadre = _id;

        } else if (sexoABuscar === 'M') {
            $("#txtNombrePadre").val(anchorItem.innerHTML);
            Estudiante.IdPadre = _id;
        }
        $("#ulListaPadres").html("");
        $("#divQuickSearch").hide();
    }
    $("#txtBusquedaPadres").val('');
    $("#ulListaPadres").html("");
}

Estudiante.capturarDatos = function () {
    Estudiante.Nombre = $("#txtNombre").val();
    Estudiante.Apellidos = $("#txtApellidos").val();
    Estudiante.Telefono = $("#txtTelefono").val();
    Estudiante.FechaNacimiento = $("#txtFechaNacimiento").val();
    Estudiante.Email = $("#txtCorreo").val();
    Estudiante.Direccion = $("#txtDireccion").val();
    Estudiante.Sexo = $("#sltSexo option:selected").val();
    Estudiante.IdCurso = $("#sltCursos option:selected").val();
}

Estudiante.registrar = function () {
    Estudiante.capturarDatos();
    let datos = JSON.stringify(Estudiante);
    console.log(datos);
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=estudiante&action=registrar',
        data: 'datos=' + datos + '',
        success: function (result) {
            DialogPanel.hide("divEdicionEstudiante");
            Estudiante.buscar();
            setTimeout(function () {
                alert(result);
            }, 500);
        }
    });
}

Estudiante.buscar = function () {
    let busqueda = $("#txtBusqueda").val();
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=estudiante&action=buscar',
        data: 'busqueda=' + busqueda + '',
        success: function (result) {
            let html = "";
            result = JSON.parse(result);
            result.reverse();
            for (a in result) {
                let n = parseInt(a);
                n += 1;
                html += '<tr>';
                html += '<td>' + n + '</td>';
                html += '<td>' + result[a].Nombre + '  ' + result[a].Apellidos + '</td>';
                html += '<td>' + result[a].Curso + '</td>';
                html += '<td>' + result[a].Telefono + '</td>';
                html += '<td><a onclick=Estudiante.cargarDatos(' + result[a].Id + ')><img src="site_media/svg/edit.svg" alt="" title="Editar" class="img-icon-small"></a></td>';
                html += "</tr>";
            }
            $("#tbodyEstudiantes").html(html);
            $("#txtBusqueda").val("");
            $("#txtBusqueda").focus();
        }
    });
}

Estudiante.cargarDatos = function (_id) {
    $.ajax({
        type: "POST",
        url: "index.php?ctrl=estudiante&action=detalle",
        data: 'id=' + _id + '',
        success: function (result) {
            result = JSON.parse(result);
            DialogPanel.show('divEdicionEstudiante');
            $("#txtId").val(result.Id);
            $("#txtNombre").val(result.Nombre);
            $("#txtApellidos").val(result.Apellidos);
            $("#txtTelefono").val(result.Telefono);
            $("#txtCorreo").val(result.Correo);
            $("#txtDireccion").val(result.Direccion);
            $("#txtNombrePadre").val(result.NombrePadre);
            $("#txtNombreMadre").val(result.NombreMadre);
            $("#txtFechaNacimiento").val(result.FechaNacimiento);
            Estudiante.Id = result.Id;
            Util.setSelectedByValue("sltSexo", result.Sexo);
            Estudiante.IdMadre = result.IdMadre;
            Estudiante.IdPadre = result.IdPadre;
            Util.setSelectedByValue("sltCursos", result.IdCurso);
            $("#sltCursos").prop("disabled", true);
            $("#h3DialogTitle").html("Edición de alumno");
        }
    });
}

Estudiante.actualizarDatos = function () {
    Estudiante.capturarDatos();
    let datos = JSON.stringify(Estudiante);
    console.log(datos);
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=estudiante&action=actualizar',
        data: 'datos=' + datos + '',
        success: function (result) {
            DialogPanel.hide("divEdicionEstudiante");
            Util.restoreFields("divEdicionEstudiante");
            Estudiante.buscar();
            setTimeout(function () {
                alert(result);
            }, 500);
        }
    });
}