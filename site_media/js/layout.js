$(document).ready(function () {
    $("#aMuestraMenuUsuario").on("click", function () {
        $("#divContMenuUsuario").toggle();
        event.stopPropagation();
    });
    //
    $("html").click(function () {
        $("#divContMenuUsuario").hide();
        $(".quick-search ").hide();
        $(".quick-search ul").html("");
    });
    //
    $(".quick-search").click(function () {
      event.stopPropagation();
    })
    //
    $("#aMenuLauncher").click(function () {
        $("#mainContainer").css("transition", "padding 0.1s");

        if ($("#divSidePanel").css("display") === 'block') {
            $("#mainContainer").css("paddingLeft", "0");
        } else {
            $("#mainContainer").css("paddingLeft", "200px");
        }
        $("#divSidePanel").toggle(200);
    });

    // $(".cancel").click(function () {
    //     $(".dialog-panel-box").slideUp(200);
    // });
});

