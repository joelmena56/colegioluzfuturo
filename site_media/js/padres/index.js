$(function () {
    buscarPadres();
    $("#bntNuevoEstudiante").click(function () {
        DialogPanel.show("divEdicionDatos");
    });

    $("#btnGuardar").click(function () {
        let tutor = recolectarDatos();
        if (tutor.Id == 0) {
            if (Util.requiredValidation("divEdicionDatos")) {
                guardarTutor(JSON.stringify(tutor));
            }
        } else {
            if (Util.requiredValidation("divEdicionDatos")) {
                actualizarDatos(JSON.stringify(tutor));
            }

        }
    })

    $("#btnCancelar").click(function(){
        DialogPanel.hide("divEdicionDatos");
    });

    $("#btnBuscar").click(function () {
        buscarPadres();
    });


    $("#txtBusqueda").on('keyup', function (e) {
        if (e.keyCode == 13) {
            buscarPadres();
        }
    });
});

function recolectarDatos() {
    let tutor = new Object();
    tutor.Id = $("#txtId").val();
    tutor.Nombre = $("#txtNombre").val();
    tutor.Apellidos = $("#txtApellidos").val();
    tutor.Telefono = $("#txtTelefono").val();
    tutor.Cedula = $("#txtCedula").val();
    tutor.Correo = $("#txtCorreo").val();
    tutor.Direccion = $("#txtDireccion").val();
    tutor.Sexo = $("#sltSexo option:selected").val();
    return tutor;
}

function guardarTutor(_datos) {
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=padres&action=registrar',
        data: 'datos=' + _datos + '',
        success: function (result) {
            $("#divEdicionDatos").slideUp(150);
            buscarPadres();
            Util.clearFields();
        }
    });
}
//
function buscarPadres() {
    var busqueda = $("#txtBusqueda").val();
    let html = "";
    $.ajax({
        type: "POST",
        url: "index.php?ctrl=padres&action=buscar",
        data: 'busqueda=' + busqueda + '',
        success: function (result) {
            result = JSON.parse(result);
            result.reverse();
            for (a in result) {
                let n = parseInt(a);
                n += 1;
                let p = JSON.parse(result[a]);
                html += '<tr>';
                html += '<td>' + n + '</td>';
                html += '<td>' + p.Nombre + '</td>';
                html += '<td>' + p.Apellidos + '</td>';
                html += '<td>' + p.Cedula + '</td>';
                html += '<td>' + p.Telefono + '</td>';
                html += '<td><a onclick=cargarDatos(' + p.Id + ')><img src="site_media/svg/edit.svg" alt="" title="Editar" class="img-icon-small"></a></td>';
                html += "</tr>";
            }
            $("#tbodyPadres").html(html);
        }
    });
}
//
function actualizarDatos(_datos) {
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=padres&action=actualizar',
        data: 'datos=' + _datos + '',
        success: function (result) {
            $("#divEdicionDatos").slideUp(150);
            buscarPadres();
            Util.clearFields();
        }
    });
}
//
function cargarDatos(_id) {
    $.ajax({
        type: "POST",
        url: "index.php?ctrl=padres&action=detalle",
        data: 'id=' + _id + '',
        success: function (result) {
            result = JSON.parse(result);

            $("#txtId").val(result.Id);
            $("#txtNombre").val(result.Nombre);
            $("#txtApellidos").val(result.Apellidos);
            $("#txtTelefono").val(result.Telefono);
            $("#txtCedula").val(result.Cedula);
            $("#txtCorreo").val(result.Correo);
            $("#txtDireccion").val(result.Direccion);

            Util.setSelectedByValue("sltSexo", result.Sexo);
            DialogPanel.show("divEdicionDatos");
        }
    });
}
