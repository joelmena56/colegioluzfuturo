// [ Variables y objetos globales ]
Empleado = new Object();
View = new Object();

// [ Cuando el documento este completamente cargado ]
$(function () {
    $("#btnNuevoEmpleado").click(function () {
        DialogPanel.show("divEdicionDatos");
    });

    $("#txtBusqueda").on('keyup', function (e) {
        if (e.keyCode == 13) {
            Empleado.buscar();
        }
    });

    $("#btnBuscar").click(Empleado.buscar);

    $("#btnCancelar").click(function () {
        DialogPanel.hide("divEdicionDatos");
    })

    $("#btnGuardar").click(function () {
        if (Util.requiredValidation("divEdicionDatos")) {
            if (Empleado.Id) {
                Empleado.actualizarDatos();
            } else {
                Empleado.registrar();
            }
        }
    });

    $("#sltPuesto").change(function () {
        if ($("#sltPuesto option:selected").val() === '1') {
            $("#gbxInfoDocente").css("display", 'inline-block');
            window.location = "#"
        } else {
            $("#gbxInfoDocente").css("display", 'none');
        }
    });

    $('#txtFechaInicio').datepicker({
        format: "dd/mm/yyyy",
        changeMonth: true,
        changeYear: true,
        todayBtn: "linked",
        language: "es",
        autoclose: true,
        todayHighlight: true
    });
    View.cargarDatos();
});
//
View.cargarDatos = function () {
    Empleado.buscar();
}
//
Empleado.registrar = function () {
    Empleado.capturarDatos();
    let datos = JSON.stringify(Empleado);
    console.log(datos);
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=empleado&action=registrar',
        data: 'datos=' + datos + '',
        success: function (result) {
            DialogPanel.hide("divEdicionDatos");
            Empleado.buscar();
            setTimeout(function () {
                alert(result);
            }, 500);
        }
    });
}

Empleado.capturarDatos = function () {
    Empleado.Nombre = $("#txtNombre").val();
    Empleado.Apellidos = $("#txtApellidos").val();
    Empleado.Telefono = $("#txtTelefono").val();
    Empleado.Cedula = $("#txtCedula").val();
    Empleado.Sexo = $("#sltSexo option:selected").val();
    Empleado.Correo = $("#txtCorreo").val();
    Empleado.Direccion = $("#txtDireccion").val();
    Empleado.IdPuesto = $("#sltPuesto option:selected").val();
    Empleado.FechaInicio = $("#txtFechaInicio").val();
    Empleado.IdAreaDocente = $("#sltAreaDocente option:selected").val();
    Empleado.IdNivelEscolar = $("#sltNivelEscolar option:selected").val();
    return Empleado;
}

Empleado.buscar = function () {
    let busqueda = $("#txtBusqueda").val();
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=empleado&action=buscar',
        data: 'busqueda=' + busqueda + '',
        success: function (result) {
            let html = "";
            result = JSON.parse(result);
            result.reverse();
            for (a in result) {
                let n = parseInt(a);
                n += 1;
                html += '<tr>';
                html += '<td>' + n + '</td>';
                html += '<td>' + result[a].Nombre + '  ' + result[a].Apellidos + '</td>';
                html += '<td>' + result[a].Telefono + '</td>';
                html += '<td>' + result[a].Puesto + '</td>';
                html += '<td><a onclick=Empleado.editar(' + result[a].Id + ')><img src="site_media/svg/edit.svg" alt="" title="Editar" class="img-icon-small"></a></td>';
                html += "</tr>";
            }
            $("#tbodyEmpleados").html(html);
            $("#txtBusqueda").val("");
            $("#txtBusqueda").focus();
        }
    });
}

Empleado.editar = function (_id) {
    Empleado.cargarDatos(_id);
}

Empleado.cargarDatos = function (_id) {
    $.ajax({
        type: "POST",
        url: "index.php?ctrl=empleado&action=detalle",
        data: 'id=' + _id + '',
        success: function (result) {
            result = JSON.parse(result);
            DialogPanel.show('divEdicionDatos');
            Empleado.Id = result.Id;
            $("#txtNombre").val(result.Nombre);
            $("#txtApellidos").val(result.Apellidos);
            $("#txtTelefono").val(result.Telefono);
            $("#txtCorreo").val(result.Correo);
            $("#txtCedula").val(result.Cedula);
            $("#txtDireccion").val(result.Direccion);
            $("#txtFechaInicio").val(result.FechaInicio);

            Util.setSelectedByValue("sltSexo", result.Sexo);
            Util.setSelectedByValue("sltPuesto", result.IdPuesto);
            Util.setSelectedByValue("sltAreaDocente", result.IdPuesto);
            Util.setSelectedByValue("sltNivelEscolar", result.IdPuesto);

            if (result.IdPuesto === '1') {
                $("#gbxInfoDocente").css("display", "inline-block");
            }

            $("#h3DialogTitle").html("Edicion de empleado");
        }
    });
}

Empleado.actualizarDatos = function () {
    Empleado.capturarDatos();
    let datos = JSON.stringify(Empleado);
    console.log(datos);
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=empleado&action=actualizar',
        data: 'datos=' + datos + '',
        success: function (result) {
            DialogPanel.hide("divEdicionDatos");
            Empleado.buscar();
            setTimeout(function () {
                alert(result);
            }, 500);
        }
    });
}

