//[ Variables globales ]
var Curso = new Object();
var View = new Object();
var Horario = new Object();
Asistencia = new Object();

// [ Cuando el documento termine de cargar]
$(function () {
    $('#txtFechaAsistencias').datepicker({
        format: "dd/mm/yyyy",
        changeMonth: true,
        changeYear: true,
        todayBtn: "linked",
        language: "es",
        autoclose: true,
        todayHighlight: true,
    });


    $("#btnNuevo").click(function () {
        DialogPanel.show("divEdicionDatos");
    })
    $("#abusquedaProfesores").click(View.busquedaProfesores);

    var timeout;
    $("#txtBusquedaProfesores").on("input propertychange", function () {
        clearTimeout(timeout);
        timeout = setTimeout(Horario.buscarProfesores, 100);
    })


    $("#btnGuardar").click(function () {
        if (Util.requiredValidation("cbbxDatosCurso")) {
            Curso.registrar();
        } else {
            alert("Hay campos requeridos que deben ser llenados");
        }
    });

    $("#btnCancelar").click(function () {
        DialogPanel.hide("divEdicionDatos");
    });

    $("#btnBuscar").click(Curso.buscar);

    $("input[name=TabItem]").click(function () {
        $("#divTabs .tab-page").hide();
        let val = $("input[name=TabItem]:checked").val();
        switch (val) {
            case 'horarios':
                $("#divHorarios").fadeIn();
                break;
            case 'listado':
                $("#divListado").fadeIn();
                break;
        }
    })

    $("#btnAgregarHorario").click(function () {
        if (Horario.IdProfesor) {
            Horario.registrar();
        } else {
            alert("Seleccione el profesor antes de continuar");
        }
    });

    $("#btnVerListadoHorarios").click(function () {
        Asistencia.verListado();
    })

    View.cargarDatos();
});

function editarHorarios() {
    $("#divEdicionHorarios").slideDown();
}

// [ Funciones principales ] 

Curso.capturarDatos = function () {
    Curso.Nombre = $("#txtNombreCurso").val();
    Curso.IdAula = $("#sltAula option:selected").val();
    Curso.IdTanda = $("#sltTanda option:selected").val();
    Curso.IdCiclo = $("#sltCiclo option:selected").val();
    return (Curso);
}

Curso.registrar = function () {
    Curso.capturarDatos();
    let curso = JSON.stringify(Curso);
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=curso&action=registrar',
        data: 'datos=' + curso + '',
        success: function (result) {
            DialogPanel.hide("divEdicionDatos");
            Curso.buscar();
            setTimeout(() => {
                alert(result);
            }, 300);
        },
        error: function () {
            alert("Error al intentar registrar Curso");
        }
    });
}

Curso.buscar = function () {
    let busqueda = $("#txtBusqueda").val();
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=curso&action=buscar',
        data: 'busqueda=' + busqueda + '',
        success: function (result) {
            let html = "";
            result = JSON.parse(result);
            result.reverse();

            for (const a in result) {
                html += '<tr>';
                html += '<td>' + result[a].Nombre + '</td>';
                html += '<td>' + result[a].Aula + '</td>';
                html += '<td>' + result[a].Tanda + '</td>';
                html += '<td>' + result[a].Ciclo + '</td>';
                html += '<td>'
                html += '<a onclick="Curso.editar(' + result[a].Id + ');">';
                html += '<img src="site_media/svg/documents.svg" alt="" title="Abrir" class="img-icon-small">';
                html += '</a>';
                html += '</td>';
                html += "</tr>";
            }

            $("#tbodyCursos").html(html);
        },
        error: function () {
            alert("Hubo un problema con la solicitud al servidor");
        }
    });
}

View.cargarDatos = function () {
    Curso.buscar();
}

Curso.editar = function (_id) {
    Curso.Id = _id;
    Curso.cargarDatos();
    Horario.cargarLista();
    $("#divTabs").show();
    $('#txtFechaAsistencias').datepicker('setDate', '-0d');
    DialogPanel.show("divEdicionDatos");
}

Curso.cargarDatos = function () {
    $.ajax({
        type: "POST",
        url: "index.php?ctrl=curso&action=detalle",
        data: 'id=' + Curso.Id + '',
        success: function (result) {
            result = JSON.parse(result);
            $("#txtNombreCurso").val(result.Nombre);
            Util.setSelectedByValue("sltAula", result.IdAula);
            Util.setSelectedByValue("sltTanda", result.IdTanda);
            Util.setSelectedByValue("sltCiclo", result.IdCiclo);

            if (result.IdPuesto === '1') {
                $("#gbxInfoDocente").css("display", "inline-block");
            }

            $("#h3DialogTitle").html("Curso");
        }
    });
}

View.busquedaProfesores = function () {
    event.stopPropagation();
    Horario.capturarDatos();
    if ($("#sltAsignatura option:selected").val() !== '0') {
        if ($("#sltDias option:selected").val() !== '0') {
            if ($("#txtHora").val() !== '') {
                $("#divQuickSearch").slideDown();
                $("#txtBusquedaProfesores").focus();
                $("#txtBusquedaProfesores").val("");
                Horario.buscarProfesores();
            } else {
                window.alert("Por favor introduzca la hora");
            }
        } else {
            window.alert("Por favor seleccione el día");
        }
    } else {
        window.alert("Por favor seleccione una asignatura");
    }
}

Horario.buscarProfesores = function () {
    let datos = Horario;
    datos.Busqueda = $("#txtBusquedaProfesores").val();
    datos = (JSON.stringify(datos));
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=empleado&action=buscarParaAsignar',
        data: 'datos=' + datos + '',
        success: function (result) {
            let html = "";
            result = JSON.parse(result);
            for (a in result) {
                let nombre = result[a].Nombre + " " + result[a].Apellidos;
                html += '<li> <a onclick=View.seleccionarProfesor(' + result[a].Id + ',this);>' + nombre + '</a>';
            }
            $("#ulListaProfesores").html(html);
        }
    });
}

View.seleccionarProfesor = function (_id, _a) {
    Horario.IdProfesor = _id;
    $("#txtNombreProfesor").val(_a.innerHTML);
    $("#divQuickSearch").slideUp(50);
}

Horario.capturarDatos = function () {
    Horario.IdCurso = Curso.Id;
    Horario.IdAsignatura = $("#sltAsignatura option:selected").val();
    Horario.Dia = $("#sltDias option:selected").val();
    Horario.Hora = $("#txtHora").val();
}

Horario.registrar = function () {
    Horario.capturarDatos();
    let datos = JSON.stringify(Horario);
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=curso&action=registrarHorario',
        data: 'datos=' + datos + '',
        success: function (result) {
            Horario.cargarLista();
            alert(result);
        },
        error: function () {
            alert("Error al intentar registrar Horario");
        }
    });
}

Horario.cargarLista = function () {
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=curso&action=listaHorarios',
        data: 'id=' + Curso.Id + '',
        success: function (result) {
            let html = "";
            result = JSON.parse(result);
            for (const a in result) {
                html += '<tr>';
                html += '<td>' + result[a].NombreAsignatura + '</td>';
                html += '<td>' + result[a].NombreProfesor + '</td>';
                html += '<td>' + result[a].Dia + '</td>';
                html += '<td>' + result[a].Hora + '</td>';
                html += '<td>'
                html += '<a onclick="Horario.eliminar(' + result[a].Id + ');">';
                html += '<img src="site_media/svg/delete.svg" alt="" title="Abrir" class="img-icon-small">';
                html += '</a>';
                html += '</td>';
                html += "</tr>";
            }
            $("#tbodyHorarios").html(html);
        }
    });
}

Horario.eliminar = function (_id) {
    if (window.confirm("¿Seguro que desea eliminar este Horario de la lista?")) {
        $.ajax({
            type: "POST",
            url: 'index.php?ctrl=curso&action=eliminarHorario',
            data: 'id=' + _id + '',
            success: function (result) {
                Horario.cargarLista();
                alert(result);
            },
            error: function () {
                alert("Error al intentar eliminar Horario");
            }
        });
    }

}

Asistencia.verListado = function () {

    let fehcha = $("#txtFechaAsistencias").val();
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=curso&action=listadoAsistencia',
        data: 'idCurso=' + Curso.Id + '&fecha=' + fehcha + '',
        success: function (result) {
            let html = "";
            result = JSON.parse(result);
            for (const a in result) {
                html += '<tr>';
                html += '<td>' + result[a].Nombre + '</td>';
                html += '<td>' + result[a].Estado + '</td>';
                html += '<td>' + result[a].HoraLlegada + '</td>';
                html += '<td>'
                html += '<a onclick=Asistencia.actualizar(' + result[a].Id + ',\'A\');>';
                html += '<img src="site_media/svg/a.svg" alt="" title="Abrir" class="img-icon-small">';
                html += '</a>&nbsp;';
                html += '<a onclick="Asistencia.actualizar(' + result[a].Id + ',\'E\');">';
                html += '<img src="site_media/svg/e.svg" alt="" title="Abrir" class="img-icon-small">';
                html += '</a>&nbsp;';
                html += '<a onclick="Asistencia.actualizar(' + result[a].Id + ',\'T\');">';
                html += '<img src="site_media/svg/t.svg" alt="" title="Abrir" class="img-icon-small">';
                html += '</a>&nbsp;';
                html += '<a onclick="Asistencia.actualizar(' + result[a].Id + ',\'P\');">';
                html += '<img src="site_media/svg/checked.svg" alt="" title="Abrir" class="img-icon-small">';
                html += '</a>';
                html += '</td>';
                html += "</tr>";
            }
            $("#tbodyListadoAsistencia").html(html);

        },
        error: function () {
            alert("Error al intentar generar Listado");
        }
    });
}

Asistencia.actualizar = function (_id, _estado) {
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=curso&action=actualizarEstadoAsistencia',
        data: 'id=' + _id+ '&estado=' + _estado + '',
        success: function (result) {
            Asistencia.verListado();
        },
        error: function () {
            alert("Error");
        }
    });

}