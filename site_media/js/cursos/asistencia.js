//[ Variables globales ]
var View = new Object();
// [ Cuando el documento termine de cargar]
$(function () {
    $('#txtFecha').datepicker({
        format: "dd/mm/yyyy",
        changeMonth: true,
        changeYear: true,
        language: "es",
        autoclose: true,
        todayHighlight: true,
        yearRange: "-100:+0"
    });

    $(document).on('keyup', function (e) {
        if (e.keyCode == 27) {
            $("#divPanelAsistencia").slideUp(100);
        }
    });
});

View.mostrarPanel = function () {
    $("#divPanelAsistencia").slideDown(100);
}

