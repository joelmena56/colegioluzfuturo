var Util = new function () {
    //
    this.requiredValidation = function (_idContainer) {
        var valid = true;
        $('#' + _idContainer + ' input[type=text]').each(
            function () {
                if ($(this).prop('required') && $(this).val() === '') {
                    $(this).css("border-color", "red");
                    $(this).click(function () {
                        $(this).css("border-color", "#DEDEDE");
                    })
                    valid = false
                }
            }
        );

        $('#' + _idContainer + ' select.required').each(function () {
            $(this).each(function () {
                var slt = $(this);
                for (const x in slt) {
                    if (slt.hasOwnProperty(x)) {
                        const element = slt[x];
                        for (const z in element) {
                            if (element.hasOwnProperty(z)) {
                                const opt = element[z]
                                if (opt.value == '0' && opt.selected === true) {
                                    slt[x].style.borderColor = 'red';
                                    slt[x].addEventListener('click', function () {
                                        slt[x].style.borderColor = '#DEDEDE';
                                    })
                                    valid = false;
                                }
                            }
                        }
                    }
                }
            })
        });
        return valid;
    }
    //
    this.setSelectedByValue = function (_idSelect, _value) {
        let slt = document.getElementById(_idSelect);
        if (slt.length > 0) {
            for (let x in slt) {
                if (slt.options[x].value) {
                    if (_value.toString() === slt.options[x].value) {
                        slt.options[x].selected = true;
                        break;
                    }
                    if (_value.toString() === slt.options[x].value) {
                        slt.options[x].selected = true;
                        break;
                    }
                }

            }
        }
    }
    //
    this.restoreFields = function (_idContainer) {
        $('#' + _idContainer + ' input[type=text]').each(
            function () {
                $(this).val("");
                $(this).css("border-color", "lightgrey");
            }
        );
        $('#' + _idContainer + ' .hidden').hide();
        $('#' + _idContainer + '  select').each(function () {
            $(this).each(function () {
                $(this).css("border-color", "lightgrey");
                var slt = $(this);
                for (const x in slt) {
                    if (slt.hasOwnProperty(x)) {
                        const element = slt[x];
                        for (const z in element) {
                            if (element.hasOwnProperty(z)) {
                                const opt = element[z]
                                if (opt.value == '0') {
                                    opt.selected = true;
                                    break;
                                }
                            }
                        }
                    }
                }
            })
        });
    }
}

var DialogPanel = new Object();
DialogPanel.show = function (_panelId) {
    $('#' + _panelId + ' .dialog-panel').hide();
    $('#' + _panelId + '').show();
    $('#' + _panelId + ' .dialog-panel').slideDown(150);

    $(document).on('keyup', function (e) {
        if (e.keyCode == 27) {
            DialogPanel.hide(_panelId);
        }
    });
}

DialogPanel.hide = function (_panelId) {
    $('#' + _panelId + ' .dialog-panel').slideUp(100);
    Util.restoreFields(_panelId);
    setTimeout(function () {
        $('#' + _panelId + '').hide();
    }, 200);
}