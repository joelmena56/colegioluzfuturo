//[ Variables globales ]
var Aula = new Object();
var View = new Object();
var Usuario = Object();

// [ Cuando el documento termine de cargar]
$(function () {
    $("#btnAgregarAula").click(Aula.agregar);

    $("#btnNuevoUsuario").click(function () {
        DialogPanel.show("divEdicionUsuario");
    });
    $("#btnGuardarUsuario").click(function () {
        Usuario.registrar();
    });
    $("#btnCancelar").click(function () {
        DialogPanel.hide("divEdicionUsuario");
    })

    $("input[name=TabItem]").click(function () {
        $("#divTabs .tab-page").hide();
        let val = $("input[name=TabItem]:checked").val();
        switch (val) {
            case 'aulas':
                $("#divAulas").fadeIn();
                break;
            case 'usuarios':
                $("#divUsuarios").fadeIn();
                Usuario.buscar();
                break;
        }
    })
    $("#btnBuscarUsuarios").click(function () {
        Usuario.buscar();
    });

    $("#aExaminarContactos").click(View.busquedaContactos);
    var timeout;
    $("#txtBusquedaContactos").on("input propertychange", function () {
        clearTimeout(timeout);
        timeout = setTimeout(Usuario.buscarContactos, 100);
    })


    View.cargarDatos();
});

// [ Funciones principales ] 
Aula.agregar = function () {
    if ($("#txtNombreAula").val() && $("#txtUbicacion").val()) {
        Aula.Nombre = $("#txtNombreAula").val();
        Aula.Ubicacion = $("#txtUbicacion").val()

        $.ajax({
            type: "POST",
            url: 'index.php?ctrl=mantenimiento&action=agregarAula',
            data: 'nombreAula=' + Aula.Nombre + '&ubicacionAula=' + Aula.Ubicacion + '',
            success: function (result) {
                Aula.cargarLista();
                alert(result);
            },
            error: function () {
                alert("Error al intentar guardar Aula");
            }
        });

    } else {
        alert("Debe introducir el nombre y la ubicació del aula")
    }
}

Aula.cargarLista = function () {
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=mantenimiento&action=listaAulas',
        success: function (result) {
            let html = "";
            result = JSON.parse(result);
            result.reverse();
            for (a in result) {
                html += '<tr>';
                html += '<td>' + result[a].Nombre + '</td>';
                html += '<td>' + result[a].Ubicacion + '</td>';
                html += '<td><a onclick=Aula.eliminar(' + result[a].Id + ')><img src="site_media/svg/delete.svg" alt="" title="Eliminar" class="img-icon-small"></a></td>';
                html += "</tr>";
            }
            $("#tbodyAulas").html(html);
        }
    });
}

Aula.eliminar = function (_id) {
    alert(_id)
}

View.cargarDatos = function () {
    Aula.cargarLista();
}

Usuario.buscar = function () {
    let busqueda = "";
    busqueda = $("#txtBusquedaUsuarios").val();
    $.ajax({
        type: "POST",
        data: 'busqueda=' + busqueda + '',
        url: 'index.php?ctrl=mantenimiento&action=buscarUsuarios',
        success: function (result) {
            let html = "";
            result = JSON.parse(result);
            result.reverse();
            for (a in result) {
                html += '<tr>';
                html += '<td>' + result[a].Nombre + '</td>';
                html += '<td>' + result[a].TipoUsuario + '</td>';
                html += '<td><a onclick=Usuario.eliminar(' + result[a].Id + ')><img src="site_media/svg/delete.svg" alt="" title="Eliminar" class="img-icon-small"></a></td>';
                html += "</tr>";
            }
            $("#tbodyUsuarios").html(html);
            $("#txtBusquedaUsuarios").val("");
            $("#txtBusquedaUsuarios").focus();
        }
    });
}

Usuario.eliminar = function (_id) {
    alert('Eliminar usuario ' + _id + '');
}

Usuario.capturarDatos = function () {
    if (Util.requiredValidation('divEdicionUsuario')) {
        Usuario.NombreUsuario = $("#txtNombreUsuario").val();
        Usuario.Password = $("#txtPassword").val();
        Usuario.IdTipoUsuario = $("input[name=TipoUsuario]:checked").val();
    } else {
        alert("Por favor complete los campos vacíos requeridos");
    }
}
Usuario.registrar = function () {
    Usuario.capturarDatos();
    console.log(Usuario);
    let datos = JSON.stringify(Usuario);
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=mantenimiento&action=registrarUsuario',
        data: 'datos=' + datos + '',
        success: function (result) {
            alert(result);
            DialogPanel.hide("divEdicionUsuario");
            Usuario.buscar();
            setTimeout(() => {
                alert(result);
            }, 300);
        },
        error: function () {
            alert("Error al intentar registrar Usuario");
        }
    });
}

Usuario.buscarContactos = function () {

    let busqueda = $("#txtBusquedaContactos").val();
    $.ajax({
        type: "POST",
        url: 'index.php?ctrl=mantenimiento&action=buscarContactos',
        data: 'busqueda=' + busqueda + '',
        success: function (result) {
            let html = "";
            result = JSON.parse(result);
            for (a in result) {
                let nombre = result[a].Nombre + " " + result[a].Apellidos;
                html += '<li> <a onclick=View.seleccionarContacto(' + result[a].Id + ',this);>' + nombre + '</a>';
            }
            $("#ulListaContactos").html(html);
        }
    });
}

View.busquedaContactos = function () {
    $("#txtBusquedaContactos").val("");
    $("#divQuickSearch").slideDown();
    $("#txtBusquedaContactos").focus();
    event.stopPropagation();
}

View.seleccionarContacto = function (_id, _a) {
    Usuario.IdPersona = _id;
    $("#txtNombreContacto").val(_a.innerHTML);
    $("#divQuickSearch").slideUp(50);
}